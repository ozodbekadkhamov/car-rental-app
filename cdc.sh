#!/bin/bash

# Check if the correct number of arguments are provided
if [ $# -ne 2 ]; then
    echo "Usage: $0 <commit-hash> <date>"
    exit 1
fi

commit=$1
datecal=$2
temp_branch="temp-rebasing-branch"
current_branch="$(git rev-parse --abbrev-ref HEAD)"

# Check if datecal is set and not empty
if [[ -z "$datecal" ]]; then
    echo "datecal is not set or is empty"
    exit 1
fi

# Convert the date to the correct format
date_timestamp=$(date -j -f "%Y-%m-%d %H:%M:%S" "$datecal" +%s)
date_r=$(date -j -f "%Y-%m-%d %H:%M:%S" "$datecal" +"%a, %d %b %Y %T %z")

# Checkout to a temporary branch at the commit you want to change
git checkout -b "$temp_branch" "$commit"

# Amend the commit date
GIT_COMMITTER_DATE="$date_timestamp" GIT_AUTHOR_DATE="$date_timestamp" git commit --amend --no-edit --date "$date_r"

# Checkout back to the original branch
git checkout "$current_branch"

# Rebase the changes onto the original branch
git rebase --committer-date-is-author-date "$commit" --onto "$temp_branch"

# Delete the temporary branch
git branch -d "$temp_branch"

# Push the changes to GitLab
git push origin "$current_branch" --force